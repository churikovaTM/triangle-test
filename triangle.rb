require "active_support/core_ext"

def triangle(a, b, c)
  t = Triangle.new(a, b, c)
  ttype = t.type
  valid_or_not = t.is_triangle_valid
  !valid_or_not ? ttype = 0 : 'g'
  return valid_or_not, ttype
end

class Triangle
  def initialize(a, b, c)
    @sides = [a, b, c].sort
  end

  def type
    case @sides.uniq.size
    when 1 then 2 #:equilateral 
    when 2 then 1 #:isosceles
    else 3 #:scalene
    end
  end

  def is_triangle_valid
    a = @sides[0].to_i
    b = @sides[1].to_i
    c = @sides[2].to_i
    s = (a + b + c) / 2.0
    ok = (s - a) * (s - b) * (s - c)
    if a <= 0 || b <= 0 || c <= 0 || ok <= 0 then
      return false
    else
      return true
    end
  end
end

sa = ARGV[0]
sb = ARGV[1]
sc = ARGV[2]

array = triangle(sa, sb, sc)

hash = {triangle: array[0], type: array[1]}
puts hash.to_xml(root: "result")
