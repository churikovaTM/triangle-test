require 'active_support/core_ext/hash/conversions'
require 'nokogiri'
load 'lib/cases.rb'

static_values = NEGATIVE[:invalid_legs]
def triangle_test(list)
  list.each do |sublist|
    puts sublist.join(' ')
    #Instead of that I sent "GET" request. 
    #For this I would use HTTPparty gem
    # result = HTTParty.get (http://appsever/triangle.php?legs?#{sublist})
    result = `ruby triangle.rb #{sublist[0][0]} #{sublist[0][1]} #{sublist[0][2]}`
    hash_result = Hash.from_xml(result) 
    puts hash_result
    if hash_result["result"]["triangle"]==sublist[1][0] && hash_result["result"]["type"]==sublist[1][1]
        puts "Test passed"
      else 
        puts "Test failed"
    end
  end
end
triangle_test(static_values)

def triangle(a, b, c)
  t = Triangle.new(a, b, c)
  ttype = t.type
  valid_or_not = t.is_triangle_valid
  !valid_or_not ? ttype = 0 : 'g'
  return valid_or_not, ttype
end

class Triangle
  def initialize(a, b, c)
    @sides = [a, b, c].sort
  end

  def type
    case @sides.uniq.size
    when 1 then 2 #:equilateral 
    when 2 then 1 #:isosceles
    else 3 #:scalene
    end
  end

  def is_triangle_valid
  	a = @sides[0]
  	b = @sides[1]
  	c = @sides[2]
  	s = (a + b + c) / 2.0
  	ok = (s - a) * (s - b) * (s - c)
  	if a <= 0 || b <= 0 || c <= 0 || ok <= 0 then
  		return false
  	else
  		return true
  	end
  end
end

random_var = Random.new
sa = rand(1000)
sb = rand(1000)
sc = rand(1000)

array = triangle(sa, sb, sc)

#puts array

dynamic_values = [[[sa, sb, sc], [array[0], array[1].to_i]]]
triangle_test(dynamic_values)




