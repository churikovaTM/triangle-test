

POSITIVE = {
	equilateral: [[[1, 2, 2], [true, 1]], [[3, 3, 4], [true, 1]], [[20000, 20000, 80000], [true, 1]]],
	isosceles: [[[1, 1, 1], [true, 2]], [[0.5, 0.5, 0.5], [true, 2]]],
	scalene: [[[3, 7, 5], [true, 3]], [[8, 9, 18], [true, 3]]]
}
NEGATIVE = {
  invalid_legs: [[[100, 100, 300], [false, 0]],[[1, 2, 300], [false, 0]]],
  contains_0: [[[0, 1, 1], [false, 0]], [[20, 200, 0], [false, 0]]]
}