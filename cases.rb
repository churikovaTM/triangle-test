module Static

POSITIVE = {
	equilateral: [[[1, 2, 2], [true, 1]], [[3, 3, 4], [true, 1]], [[20000, 20000, 80000], [true, 1]]]
	isosceles: [[[1, 1, 1], [true, 2]], [[0.5, 0.5, 0.5], [true, 2]]]
	scalene: [[[3, 7, 5], [true, 3]], [[8, 9, 18], [true, 3]]]
}